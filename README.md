Project 4 Specification
=======================

Project 1 & 2 required a program that could be taught facts by a user
and answer simple questions.

Project 3 required a program that could teach users facts and ask
users questions about facts.

In Project 4 these abilities will be combined and extended; the program
should teach facts about the world (e.g. Fido is a dog; John has a
dog), ask questions about facts (e.g. Does John have a dog?) and answer
questions about facts.

User input will a series of lines of space-separated words (possibly
terminated by some punctuation mark) read from standard input.
Responses will be of the same form written to stout.

You must implement this project in python3.

The project skeleton is available on [bitbucket](https://bitbucket.org/jtcb/cmsc421-project4). We strongly recommend you use some kind of version control - the easiest way to do this is to fork the cmsc421-project4 skeleton.

**Added November 24th**: Your project should *run* after executing

    $ python3 project4.py
    
at the shell. (e.g. We shouldn't have to start the python REPL, import the project, and then execute some main function by hand.) The project skeleton already does this. Many project3's did *not* which is why I bring it up now.

Policy on use of sources
------------------------

There is a lot of previous NLP work, including grammars and parsers.
You are welcome to consult any such material, to get ideas (including 
technical ideas on coding). ***However,***
 
1.) You must fully implement your own algorithm yourself, and **never**
simply copy existing code.

2.) You must fully acknowledge any code or algorithm or other sources
that you borrow ideas from.

(Note that Google makes it easy to check copying.)

You will not be penalized for borrowing ideas; this is encouraged
when it is relevant. However, take care to avoid spending too much time
studying/implementing interesting ideas that are not relevant to the
project.

Minimum Requirements
--------------------

Your project 4 implementation should be designed so it can communicate with other project 4 implementations (one way to test this during development is to run two instances of your own project and have them communicate with each other). ***At a minimum***, the following communication rules should be satisfied:

### Output format ###

***Read this section very carefully.***

Your program should emit two types of output sentences.

A **fact** sentence is always of the form:

    [subject] [verb] [object]
    
where [subject] is *not* an [interrogative pronoun](http://en.wiktionary.org/wiki/interrogative_pronoun). [subject] may be a compound subject (e.g. Josh and Emily are TAs).

"I" is a special subject. (See the next section.)

A **query** is any non-fact sentence that can be reasonably answered as a fact. For example:

    Who is a TA?
    Is Josh a TA?
    Are TAs employees?

might be answered as:

    Josh and Emily are TAs.
    Josh is a TA.
    TAs are employees.
    
Anything that isn't a **fact** or **query** is malformed input/output. (Regardless of the input that your program receives, it should never respond with malformed output.)
    
### Input/Output rules ###

***Read this section very carefully.***

Your program should follow an input-output cycle according to the following rules (designed to maximize the knowledge in your program and the other program's knowledge base).

When your program is started, it must begin by emitting a fact.

When your program receives a **query** it must attempt to answer correctly with a fact. 

    Who is a dog? => Snoopy is a dog.
    
If the program does not have the required information in its knowledge base, it may attempt to respond with a *different* related query that might help answer the original query in the future 

    Who is a dog? => Is Snoopy a dog? => Snoopy is a dog. => ...
    
Alternatively, it may respond with "I am unsure."

When your program receives a *new* **fact** it should attempt to incorporate it into its knowledge base. Your program should attempt to respond with a related query...

    Dogs are cute. => What are dogs? => Dogs are mammals. => Is Josh a mammal? => ...
    
...in an attempt to gain additional information. It should *not* respond with a query that is unlikely to result in new information (the following is *bad* behavior):

    Snoopy is a dog. => Is Snoopy a dog?
    
A small percentage (<< 1/2 of the time), your program may respond to a *new* fact with a fact of it's own, but remember the goal is to start a dialog. Two programs just emitting facts is not a dialog.

When your program receives a **fact** *already* in its knowledge base, it may respond with a fact or a query with about a 50% probability of either; the exact probability does not matter, we just don't want to see programs getting 'stuck' doing nothing but answering questions or giving facts.

If your program is unable to parse a query or fact (e.g. malformed input), it should respond with "I am confused."

When your program receives "I am confused." or "I am unsure." it may either attempt to clarify with different but related facts or queries or completely change topics with an unrelated fact or query. A sophisticated program will attempt to clarify a few times, before giving up and moving on. A less sophisticated, but still acceptable solution is to randomly decide whether to give a related fact (in an attempt to clarify) or unrelated fact.

After having emitted at least 20 outputs, your program may choose to respond at any time with "I am leaving." then terminate. Your program should respond to the input "I am leaving." by terminating as well.

Regardless of input, your program should always attempt to gracefully continue processing. ***Do not allow uncaught exceptions to propagate to the top-level.***

### Lexicon ###

At a bare minimum, your program should be able to recognize fact and query forms similar to the following and conduct a minimal level of inference.

    Fido is a dog.
    Dogs are animals.
    Is Fido a dog?
    Is Fido an animal?
    Are dogs animals?
    Who is a dog?
    What is Fido?

It is acceptable to assume that the plural of any noun is that noun followed by an "s". (e.g. persons, foots)

Your program will also need it's own initially instantiated knowledge base so it can ask questions. The knowledge base can be about anything you want, just keep it rated PG.

Additional Features
-------------------

The above are *minimum* requirements, fulfilling just the minimum will probably be worth a C grade.

Below is a list of possible additional features to implement (if you think of other features you would like to implement, suggest them on piazza). A grade B project will implement 2-3 of these. A grade A project will implement more.

+ Compound objects (What is Josh => Josh is a student and a TA.)

+ Better plural nouns (What is the plural of foot? => The plural of foot is feet. => Are feet appendages? => ...)

+ Additional verbs (e.g. has, have, can, etc...)

+ Adjectives (Snoopy is a cute dog. => Who is cute? => Snoopy is cute. => Who is a dog? => Snoopy is a dog...)

+ Adverbs (Josh can run quickly. => Who can run? => Josh and John can run. => Who can run quickly? => Josh can run quickly.)

+ Prepositions (The sky is above the ground. => Where is the ground? => The ground is below the sky.)

+ Numerical adjectives (Josh has 2 videogames. => ... => Emily has 3 videogames. => ... => Who has the most videogames? => Emily has 3 videogames.)

Bear in mind that your program cannot always rely on it's conversation partner to act the way it wants. For example, your program might ask "What is the plural of foot?" and get back "I am unsure." Your program will have to continue with some 'default' value in its knowledge base.

Style
-----

See [PEP-8](http://www.python.org/dev/peps/pep-0008/) for python style conventions.

Groups
------

We suggest the following starting 'roles' for implementing this project:

+ lexicon maintenance - build the lexicon and write code to update it as the program learns new knowledge

+ sentence parsing/building - take raw input and break it down into a form that your program can process; generate sentences that will be passed to the other program

+ inference - Infer new knowledge from existing knowledge

+ dialog management - take a parsed sentence and determine the appropriate response

As you develop the project you may find that work needs to be done that doesn't exactly fit the 'roles' above; these roles are just initial suggestions.