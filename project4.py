#!/bin/python3

""" This is the project 4 skeleton file.

Put module-level documentation here. (Including your group names.)
"""

import sys

def main():
    """ The main loop of your program. """
    
    for line in sys.stdin:
        process_input(line)

def process_input(line):
    """ Program logic goes here. """
    print("I am confused.")




# This executes main() if project4.py was executed at the shell.
# Otherwise, main() will not be called (useful for debugging).
if __name__ == "__main__":
    main()
